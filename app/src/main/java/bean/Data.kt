package bean

class Data {
    private var name: String? = null
    private var imgUrl: String? = null

    fun DataModal() {
        // empty constructor required for firebase.
    }

    // constructor for our object class.
    fun DataModal(name: String?, imgUrl: String?) {
        this.name = name
        this.imgUrl = imgUrl
    }

    // getter and setter methods
    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getImgUrl(): String? {
        return imgUrl
    }

    fun setImgUrl(imgUrl: String?) {
        this.imgUrl = imgUrl
    }
}
package activities

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.dev.anivaletask.R
import com.squareup.picasso.Picasso
import util.ScrollTextView

class EnlargedImageScreen:AppCompatActivity(){

    var imageView:ImageView?=null
    var builder:AlertDialog.Builder?=null
    var progressDialog:ProgressDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.enlarged_image_screen)

        val mTopToolbar =findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mTopToolbar)
        supportActionBar!!.setTitle("Enlarged Image")
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val scrolltext = findViewById<ScrollTextView>(R.id.scrolltext)
        scrolltext.text = "AnivaleGames"
        scrolltext.startScroll()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.parseColor("#7BDEFE") //33A1C9
        }

        imageView=findViewById<ImageView>(R.id.imageView)

        val intent = intent
        val l_image = intent.getStringExtra("l_image")

        showBar()

        Picasso.with(applicationContext).load(l_image).into(imageView);
        progressDialog!!.dismiss()


    }

    fun showBar() {
        builder = AlertDialog.Builder(this@EnlargedImageScreen)
        progressDialog = ProgressDialog(this@EnlargedImageScreen)
        progressDialog!!.setMessage("Processing Data...")
        progressDialog!!.setCancelable(false)
        progressDialog!!.setTitle("Please Wait")
        progressDialog!!.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        val dashboard = Intent(baseContext, GridImagesScreen::class.java)
        dashboard.addCategory(Intent.CATEGORY_HOME)
        dashboard.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(dashboard)
        this@EnlargedImageScreen.finish()
    }

}
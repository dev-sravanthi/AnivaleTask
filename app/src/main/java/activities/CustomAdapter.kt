package activities

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.dev.anivaletask.R

class CustomAdapter(
    activity: FragmentActivity,
    var result: Array<String>,
    prgmImages: IntArray
) :
    BaseAdapter() {
    var context: Context
    var imageId: IntArray
    override fun getCount(): Int {
        // TODO Auto-generated method stub
        return result.size
    }

    override fun getItem(position: Int): Any {
        // TODO Auto-generated method stub
        return position
    }

    override fun getItemId(position: Int): Long {
        // TODO Auto-generated method stub
        return position.toLong()
    }

    inner class Holder {
        var tv: TextView? = null
        var img: ImageView? = null
    }

    override fun getView(
        position: Int,
        convertView: View,
        parent: ViewGroup
    ): View {
        // TODO Auto-generated method stub
        val holder = Holder()
        val rowView: View
        rowView = inflater!!.inflate(R.layout.programlist, null)
        holder.tv = rowView.findViewById<View>(R.id.textView1) as TextView
        holder.img =
            rowView.findViewById<View>(R.id.imageView1) as ImageView
        holder.tv!!.text = result[position]
        holder.img!!.setImageResource(imageId[position])
        //holder.img.setLayoutParams(new GridView.LayoutParams(85, 85));

        /*rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_LONG).show();
            }
        });*/return rowView
    }

    companion object {
        private var inflater: LayoutInflater? = null
    }

    init {
        context = activity
        imageId = prgmImages
        inflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}
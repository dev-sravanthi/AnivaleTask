package activities

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.GridView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.dev.anivaletask.BuildConfig
import com.dev.anivaletask.R
import org.json.JSONException
import util.AndroidUtil
import util.ScrollTextView

class GridImagesScreen:AppCompatActivity(){

    var util:AndroidUtil?=null
    var builder:AlertDialog.Builder?=null
    var progressDialog:ProgressDialog?=null
    var list_images:ArrayList<String>?=null
    var list_name:ArrayList<String>?=null
    var large_images:ArrayList<String>?=null
    var previewURL:String=""
    var user:String=""
    var largeImage:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.grid_images_screen)

        list_images= ArrayList<String>()
        list_name= ArrayList<String>()
        large_images=ArrayList<String>()

//        util = AndroidUtil
//        if (!util!!.isOnline()) {
//            builder!!.setIcon(R.drawable.ic_launcher_background)
//            builder!!.setTitle("Internet Connection")
//            builder!!.setMessage("Please Check Your Internet Connection").setPositiveButton("OK",
//                    DialogInterface.OnClickListener { dialog, which -> finish() }).create().show()
//        }

        val mTopToolbar =findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mTopToolbar)
        supportActionBar!!.setTitle("Grid Images")
        val scrolltext = findViewById<ScrollTextView>(R.id.scrolltext)
        scrolltext.text = "AnivaleGames"
        scrolltext.startScroll()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.parseColor("#7BDEFE") //33A1C9
        }

        getData()

    }

    fun getData(){

        val requestQueue = Volley.newRequestQueue(applicationContext)

        val jsonObjectRequest = JsonObjectRequest(
                Request.Method.POST,
                BuildConfig.FETCH_IMAGES,
                null,
                Response.Listener { response ->
                    try {
                        val jsonArray = response.getJSONArray("hits")
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject = jsonArray.getJSONObject(i)
                            previewURL = jsonObject.getString("previewURL")
                            user = jsonObject.getString("user")
                            largeImage=jsonObject.getString("largeImageURL")

                            list_images!!.add(previewURL)
                            list_name!!.add(user)
                            large_images!!.add(largeImage)
                        }

                        val gridview = findViewById<GridView>(R.id.gridView)

                        val adapter = ImageListAdapter(this, R.layout.programlist,list_name!!,list_images!!)
                        gridview.adapter = adapter

                        gridview.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->
                            //val selectedItem = adapter.getItemAtPosicion(position) as String
                            val a = java.lang.String.valueOf(position)
                            val l_image=large_images!!.get(java.lang.Integer.valueOf(a))
                            val i = Intent(this@GridImagesScreen, EnlargedImageScreen::class.java)
                            i.putExtra("l_image",l_image)
                            startActivity(i)
                            finish()
                        }
                    } catch (e: JSONException) {
                        if (progressDialog != null && progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    if (progressDialog != null && progressDialog!!.isShowing) {
                        progressDialog!!.dismiss()
                    }
                    val textView = TextView(this@GridImagesScreen)
                    textView.text = error.toString()
                }
        )
        requestQueue.add(jsonObjectRequest)
    }

    var exit = false

    override fun onBackPressed() {
        if (exit) {
            super.onBackPressed()
            return
        }
        exit = true
        Toast.makeText(this, "Double click back to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ exit = false }, 2000)
    }


}

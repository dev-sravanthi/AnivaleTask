package util

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

open class MyCustomTextViewBold(
    context: Context,
    attr: AttributeSet?
) :
    AppCompatTextView(context, attr) {
    init {
        this.typeface = Typeface.createFromAsset(context.assets, "Sansation-Bold.ttf")
    }
}
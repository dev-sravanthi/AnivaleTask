package util

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Scroller

class ScrollTextView : MyCustomTextViewBold{

    private var mSlr: Scroller? = null
    private var mRndDuration = 24000
    private var mXPaused = 0
    private var mPaused = true

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet){
        setSingleLine()
        ellipsize = null
        visibility = View.INVISIBLE
    }

    fun startScroll() {
        mXPaused = -1 * width
        mPaused = true
        resumeScroll()
    }
    fun resumeScroll() {

        if (!mPaused)
            return
        setHorizontallyScrolling(true)
        mSlr = Scroller(this.context, LinearInterpolator())
        setScroller(mSlr)

        val scrollingLen = calculateScrollingLen()
        val distance = scrollingLen - (width + mXPaused)
        val duration =
            (mRndDuration.toDouble() * distance.toDouble() * 1.00000 / scrollingLen).toInt()

        visibility = View.VISIBLE
        mSlr!!.startScroll(mXPaused, 0, distance, 0, duration)
        invalidate()
        mPaused = false
    }

    private fun calculateScrollingLen(): Int {
        val tp = paint
        var rect: Rect? = Rect()
        val strTxt = text.toString()
        tp.getTextBounds(strTxt, 0, strTxt.length, rect)
        val scrollingLen = rect!!.width() + width
        rect = null
        return scrollingLen
    }

    fun pauseScroll() {
        if (null == mSlr)
            return

        if (mPaused)
            return

        mPaused = true

        mXPaused = mSlr!!.currX

        mSlr!!.abortAnimation()
    }

    override

     fun computeScroll() {
        super.computeScroll()

        if (null == mSlr) return

        if (mSlr!!.isFinished && !mPaused) {
            this.startScroll()
        }
    }

    fun getRndDuration(): Int {
        return mRndDuration
    }

    fun setRndDuration(duration: Int) {
        this.mRndDuration = duration
    }

    fun isPaused(): Boolean {
        return mPaused
    }
}
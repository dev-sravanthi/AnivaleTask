package util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object AndroidUtil {

    private var con: Context? = null

    fun AndroidUtil(con: Context?) {
        this.con = con
    }

    fun AndroidUtil() {}

    fun isOnline(): Boolean {
        val connectivity = con!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null) for (i in info.indices) if (info[i].state == NetworkInfo.State.CONNECTED) {
                return true
            }
        }
        return false
    }

}